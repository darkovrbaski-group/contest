FROM python:3.8-alpine

RUN adduser user --disabled-password

USER user

WORKDIR /python-docker

COPY . .

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt --no-cache-dir --user

EXPOSE 8000

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
